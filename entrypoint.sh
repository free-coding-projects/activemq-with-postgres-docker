#!/bin/bash

dataDir=/app
if [[ -d ${dataDir} && -n "$(ls -A $dataDir)" ]]; then
  echo "Datadir exists, skip recreation until data dir is deleted"
else
  echo "Datadir dont exists, create data dir"

  envsubst < /tmp/apache-activemq-${ACTIVEMQ_VERSION}/conf/templates/db.properties.template \
         > /tmp/apache-activemq-${ACTIVEMQ_VERSION}/conf/db.properties; \
  envsubst < /tmp/apache-activemq-${ACTIVEMQ_VERSION}/conf/templates/jetty-realm.properties.templates \
         > /tmp/apache-activemq-${ACTIVEMQ_VERSION}/conf/jetty-realm.properties; \
  envsubst < /tmp/apache-activemq-${ACTIVEMQ_VERSION}/conf/templates/jmx.password.templates \
         > /tmp/apache-activemq-${ACTIVEMQ_VERSION}/conf/jmx.password;
  envsubst < /tmp/apache-activemq-${ACTIVEMQ_VERSION}/conf/templates/env.template \
         > /tmp/apache-activemq-${ACTIVEMQ_VERSION}/bin/env;

  cp -r -v /tmp/* /app
  chown -R activemq:activemq /app
  chmod -R 700 /app
  chmod -R 600 /app/apache-activemq-${ACTIVEMQ_VERSION}/conf/jmx.password
fi

echo "Running as uid=$(id -u) gid=$(id -g) with /tmp as '$(ls -lnd /tmp)'"
ls -alR /app

exec gosu activemq "$@"