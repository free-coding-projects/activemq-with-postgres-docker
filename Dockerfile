FROM eclipse-temurin:11-jdk

ENV ACTIVEMQ_VERSION=5.16.3 \
    ACTIVEMQ_POSTGRES_JDBC_DRIVER=42.3.1 \
    UID=1000 \
    GID=1000 \
    DB_HOST=amqdb \
    DB_PORT=5432 \
    DB_USER=postgres \
    DB_PASSWORD=postgres \
    DB_NAME=activemq \
    DB_INIT_CONN=5 \
    DB_MAX_CONN=10 \
    AMQ_ADMIN_PW=1234 \
    AMQ_USER_PW=0815 \
    AMQ_JMX_PW=blabla \
    DEBUG_MODE=true \
    AMQ_JMX_HOST=localhost

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive \
  apt-get install -y \
    gettext \
    wget \
    gosu \
    && apt-get clean

RUN addgroup --gid ${GID} activemq \
  && adduser --system --shell /bin/false --uid ${UID} --ingroup activemq --home /app activemq

COPY entrypoint.sh /

RUN chmod +x /entrypoint.sh; \
    chmod +x /copy_mount.sh; \
    chown activemq:activemq /entrypoint.sh

RUN set -eux; \
    mkdir -p /tmp/DLs/libs; \
    wget -O /tmp/DLs/apache-activemq-${ACTIVEMQ_VERSION}-bin.tar.gz \
                http://archive.apache.org/dist/activemq/${ACTIVEMQ_VERSION}/apache-activemq-${ACTIVEMQ_VERSION}-bin.tar.gz; \
    wget -O /tmp/DLs/libs/postgresql-${ACTIVEMQ_POSTGRES_JDBC_DRIVER}.jar \
                https://jdbc.postgresql.org/download/postgresql-${ACTIVEMQ_POSTGRES_JDBC_DRIVER}.jar; \
    tar -xzf /tmp/DLs/apache-activemq-${ACTIVEMQ_VERSION}-bin.tar.gz -C /tmp; \
    cp /tmp/DLs/libs/* /tmp/apache-activemq-${ACTIVEMQ_VERSION}/lib;

COPY conf/app/ /tmp/apache-activemq-${ACTIVEMQ_VERSION}/conf/

STOPSIGNAL SIGTERM

# ACTIVEMQ UI
EXPOSE 8161
# ACTIVEMQ TCP
EXPOSE 61616
# ACTIVEMQ JMX
EXPOSE 11099

ENTRYPOINT [ "/entrypoint.sh" ]

CMD /app/apache-activemq-${ACTIVEMQ_VERSION}/bin/activemq console