# activemq5 with postgres db

Custom Docker image which downloads a specified ActiveMQ version and connect against a PostgreSQL database.\
[![pipeline status](https://gitlab.com/free-coding-projects/activemq-with-postgres-docker/badges/master/pipeline.svg)](https://gitlab.com/free-coding-projects/activemq-with-postgres-docker/-/commits/master)

#### Build and run the Docker image

* Build the image
```
docker build -t activemq .
```

* Run with docker-compose file, which starts postgres, amq server and adminer (DB webclient)
```
docker-compose -up d
``` 
* Run with already started postgres db
```
docker run --rm -d -p 8161:8161 -p 61616:61616 activemq 
```

* Check your localhost at port `8161`

```
http://localhost:8161
```

#### Available environment variables
* ENV ACTIVEMQ_VERSION 5.16.3
* ENV ACTIVEMQ_POSTGRES_JDBC_DRIVER 42.3.1
* ENV DB_HOST amqdb
* ENV DB_PORT 5432
* ENV DB_USER postgres
* ENV DB_PASSWORD postgres
* ENV DB_NAME activemq
* ENV DB_INIT_CONN 5
* ENV DB_MAX_CONN 10
* ENV AMQ_ADMIN_PW 1234
* ENV AMQ_USER_PW 0815
* ENV AMQ_JMX_PW blabla
* ENV UID 1000
* ENV GID 1000
